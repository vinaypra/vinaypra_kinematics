# Import dependencies
import rospy, math, numpy as np
from std_msgs.msg import Float32MultiArray
#from utilities import reset_world

# Initialize the node
rospy.init_node('holonomic_controller', anonymous=True)

# Define the publisher for moving the robot
pub = rospy.Publisher('wheel_speed', Float32MultiArray, queue_size=10)

# This will make the robot move forward
forward = [1, 1, 1, 1]
msg = Float32MultiArray(data=forward)
pub.publish(msg) # Publish the data to move the robot forward

# Defining rate
rospy.sleep(5.0)

# This will make the robot stop
stop = [0, 0, 0, 0]
msg = Float32MultiArray(data=stop)
pub.publish(msg) # Publish the data to make the robot stop