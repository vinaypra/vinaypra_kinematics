#! /usr/bin/env python

# Import dependencies
import rospy
from simple_robot_gazebo.msg import encoders
import math
from math import sin, cos
import tf
from tf.broadcaster import TransformBroadcaster
from geometry_msgs.msg import Twist, Pose, Point, Quaternion, Vector3
from nav_msgs.msg import Odometry
'''
Creating a class so that there is no issues with the 
callback function this time
'''

class DiffRobotOdometry:

    # Defining init function
    def __init__(self):
        # Defining inital robot model attributes
        self.radius = 0.1       # Radius of wheel
        self.length = 0.3       # Length between wheels
        self.local_x = 0.0      # x co-ordinate in local robot frame
        self.local_y = 0.0      # y co-ordinate in local robot frame
        self.local_theta = 0.0  # Angular orientation for the robot chassis
        '''
        self.inert_x = 0.0      # x co-ordinate in inertial frame
        self.inert_y = 0.0      # y co-ordinate in inertial frame
        self.inert_theta = 0.0  # Angular orientation for the robot chassis in inertial frame
        '''
        # Defining initial encoder attributes
        self.N = 360    # Number of encoder ticks
        '''
        del(ticks) = end_tick - begin_tick
        '''
        # Defining tick variables to compute the ticks in a period for right wheel
        self.right_wheel_end_tick = 0
        self.right_wheel_begin_tick = 0
        
        # Compute the ticks in a period for left wheel
        self.left_wheel_end_tick = 0
        self.left_wheel_begin_tick = 0

        # Defining subscriber to read encoder ticks
        self.encoder_ticks_sub = rospy.Subscriber('/encoders', encoders, self.callback)

        # Defining a variable to keep track of time
        self.time_var_end = rospy.Time.now()

        # Defining odometry transform
        self.odom_broadcast = TransformBroadcaster()

        # Defining publisher for odometry data
        self.odometry_pub = rospy.Publisher('/odom', Odometry, queue_size=1)

        # Define rate
        self.rate = rospy.Rate(1)

    def callback(self, msg):
        '''
        # From the file 'encoder_ticks.cc'

        msg.encoderTicks = {(float)encoder_left, (float)encoder_right};
        msg.timeStamp = milliTime;
        '''
        '''
        Defined in encoders.msg . Turns out, the sequence of index is (refer above):
        left wheel -> encoderTicks[0]
        right wheel -> encoderTicks[1]
        '''
        self.right_wheel_end_tick = msg.encoderTicks[1]
        self.left_wheel_end_tick = msg.encoderTicks[0]
        #self.right_wheel_begin_tick = msg.encoderTicks[1]
        #self.left_wheel_begin_tick = msg.encoderTicks[0]

    def publish_odometry(self):
        while not rospy.is_shutdown():

            # Capture starting time stamp
            time_var_begin = rospy.Time.now()

            # Calculate d_r and d_l
            d_r = (2 * (3.14) * self.radius * (self.right_wheel_end_tick - self.right_wheel_begin_tick)) / self.N
            d_l = (2 * (3.14) * self.radius * (self.left_wheel_end_tick - self.left_wheel_begin_tick)) / self.N

            '''
            Update tick values for next cycle: Last tick value from 
            previous cycle = Beginning tick value for next cycle
            '''
            self.right_wheel_begin_tick = self.right_wheel_end_tick
            self.right_wheel_begin_tick = self.right_wheel_end_tick

            # Calculate v and w using given equations
            v = (d_r + d_l) / 2
            w = (d_r - d_l) / self.length

            # Calculate x_dot, y_dot and phi_dot (Canonical Simplified Model)
            x_dot = v * cos(self.local_theta)
            y_dot = v * sin(self.local_theta)
            phi_dot = w

            # Update the robot local co-ordinates
            self.local_x = x_dot + self.local_x
            self.local_y = y_dot + self.local_y
            self.local_theta = phi_dot + self.local_theta

            # Compute quaternion since pose uses quaternions
            diff_robot_quat = tf.transformations.quaternion_from_euler(0, 0, self.local_theta)

            # Publish the transform
            self.odom_broadcast.sendTransform(
                (self.local_x, self.local_y, 0.),
                diff_robot_quat,
                time_var_begin,
                "link_chassis",
                "odom"
            )

            # Publish the odometry data
            diff_rob_odometry = Odometry()
            diff_rob_odometry.header.stamp = time_var_begin
            diff_rob_odometry.header.frame_id = "odom"

            # Setting the position
            diff_rob_odometry.pose.pose = Pose(Point(self.local_x, self.local_y, 0.), Quaternion(*diff_robot_quat))

            # Setting the velocity
            diff_rob_odometry.child_frame_id = "link_chassis"
            diff_rob_odometry.twist.twist = Twist(Vector3(v, 0, 0), Vector3(0, 0, w))

            # Publishing the position and velocity
            self.odometry_pub.publish(diff_rob_odometry)

            self.time_var_end = time_var_begin
            self.rate.sleep()

if __name__ == "__main__":
    # Initialize node
    rospy.init_node('diff_robot_odometry_node')

    # Create an instance of the class
    diff_rob_odometry_obj = DiffRobotOdometry()

    # Publish the odometry data
    diff_rob_odometry_obj.publish_odometry()