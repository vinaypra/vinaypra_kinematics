#! /usr/bin/env python

# Import dependencies
import rospy, math, numpy as np
from std_msgs.msg import Float32MultiArray
#from utilities import reset_world

# Initialize the node
rospy.init_node('holonomic_controller', anonymous=True)

# Define the publisher for moving the robot
pub = rospy.Publisher('wheel_speed', Float32MultiArray, queue_size=10)

# Instructing sleep duration of 1 second
rospy.sleep(1) # Change sleep values here to increase duration of action to be executed - increasing the time will allow longer time during which action is executed
'''
From the ipynb notebook:
Forward - all wheels drive forward at the same speed with positive values.
Backward -  all wheels drive forward at the same speed with negative values.
Left translation - wheels 1 and 3 drive backward and wheels 2 and 4 drive forward at the same speed.
Right translation -  wheels 1 and 3 drive forward and wheels 2 and 4 drive backward at the same speed.
Counterclockwise rotation - wheels 1 and 4 drive backward and wheels 2 and 3 drive forward at the same speed.
Clockwise rotation - wheels 1 and 4 drive forward and wheels 2 and 3 drive backward at the same speed.
'''
# This will make the robot move backwards
backward = [-1, -1, -1, -1]
msg = Float32MultiArray(data=backward)
pub.publish(msg) # Publish the data to move the robot backwards

rospy.sleep(1)

# This will make the robot move left
left = [-1, 1, -1, 1]
msg = Float32MultiArray(data=left)
pub.publish(msg) # Publish the data to move the robot left

rospy.sleep(1)

# This will make the robot move right
right = [1, -1, 1, -1]
msg = Float32MultiArray(data=right)
pub.publish(msg) # Publish the data to move the robot right

rospy.sleep(1)

# This will make the robot move counter-clockwise
counterclockwise = [-1, 1, 1, -1]
msg = Float32MultiArray(data=counterclockwise)
pub.publish(msg) # Publish the data to move the counter-clockwise

rospy.sleep(1)

# This will make the robot move clockwise
clockwise = [1, -1, -1, 1]
msg = Float32MultiArray(data=clockwise)
pub.publish(msg) # Publish the data to move the clockwise

rospy.sleep(1)

# This will make the robot stop
stop = [0, 0, 0, 0]
msg = Float32MultiArray(data=stop)
pub.publish(msg) # Publish the data to make the robot stop

'''
# This will make the robot move forward
forward = [1, 1, 1, 1]
msg = Float32MultiArray(data=forward)
pub.publish(msg) # Publish the data to move the robot forward

# Defining rate
rospy.sleep(5.0)

# This will make the robot stop
stop = [0, 0, 0, 0]
msg = Float32MultiArray(data=stop)
pub.publish(msg) # Publish the data to make the robot stop
'''