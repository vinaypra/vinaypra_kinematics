#! /usr/bin/env python

# Import dependencies
import rospy, math, numpy as np
from math import sin, cos
from std_msgs.msg import Float32MultiArray
#from utilities import reset_world
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion

# Initialize the node
rospy.init_node('square_node', anonymous=True)

def twist2wheels(wz, vx, vy):
    '''
    Given in the ipynb notebook that,
    u = Transpose[u1 u2 u3 u4] = (1/r)* [[(-l-w), 1, -1], [(l+w), 1, 1], [(l+w), 1, -1], [(l-w), 1, 1].Transpose[w_bz v_bx v_by]
    '''
    # Plugging in the values from the ipynb notebook
    length = 0.250 # 500/2
    width = 0.274 # 548/2
    radius = 0.127 # 254/2

    # Defining intermediate_matrix for '(1/r)* [[(-l-w), 1, -1], [(l+w), 1, 1], [(l+w), 1, -1], [(l-w), 1, 1]' part
    intermediate_matrix = np.array([[(-length-width)/radius, 1/radius, -1/radius], 
                                    [(length+width)/radius, 1/radius, 1/radius], 
                                    [(length+width)/radius, 1/radius, -1/radius], 
                                    [(-length-width)/radius, 1/radius, 1/radius]])
    
    # Defining the twist_vector_matrix for storing wz, vx, vy
    twist_vector_matrix = np.array([wz, vx, vy])

    # Defining the dimensions for the twist_vector_matrix
    twist_vector_matrix.shape = (3,1)

    # Computing dot product for u, [4x3]dot[3x1] = [4x1], which is the needed dimensions for u
    u = np.dot(intermediate_matrix, twist_vector_matrix)

    return u

def odom_callback(msg):
    global phi   
    position = msg.pose.pose.position
    (_, _, phi) = euler_from_quaternion([msg.pose.pose.orientation.x, 
                                         msg.pose.pose.orientation.y, 
                                         msg.pose.pose.orientation.z, 
                                         msg.pose.pose.orientation.w])
    

def velocity2twist(dphi, dx, dy):
    '''
    Given in the ipynb notebook (Section 4.1) that,
    v_b = Transpose[w_bz v_bx v_by] = [[1, 0, 0], [0, cos phi, sin phi], [0, -sin phi, cos phi]].Transpose[phi_dot x_dot y_dot]
    '''
     # Defining intermediate_matrix for computing dot product later
    intermediate_matrix_2 = np.array([[1, 0, 0], 
                                    [0, cos(phi), sin(phi)],
                                    [0, -sin(phi), cos(phi)]])

    # Defining the twist_vector_matrix for storing dphi, dx, dy
    v_b_matrix = np.array([dphi, dx, dy])

    # Defining the dimensions for the v_b_matrix
    v_b_matrix.shape = (3,1)

     # Computing dot product for twist values for the chassis frame
     # [3x3]dot[3x1] = [3x1], which is the needed dimensions for v_b = Transpose[wz vx vy]
    twist_value = np.dot(intermediate_matrix_2, v_b_matrix)

    # Parsing values of wz, vx and vy from the twist_value_matrix
    wz = twist_value[0][0]
    vx = twist_value[1][0]
    vy = twist_value[2][0]

    return wz, vx, vy

def move_rob(dphi, dx, dy):
    for _ in range(300):    # for 3 m x 3 m square
    #for _ in range(100):    # for 1 m x 1 m square
        wz, vx, vy = velocity2twist(dphi, dx, dy)
        u = twist2wheels(wz, vx, vy)
        msg = Float32MultiArray(data=u)
        pub.publish(msg)
        rospy.sleep(0.01)
# Define the publisher for moving the robot
pub = rospy.Publisher('wheel_speed', Float32MultiArray, queue_size=10)

# Defining Subscriber to read from '/odom' topic
position_sub = rospy.Subscriber("/odom", Odometry, odom_callback)

# Instructing sleep duration of 1 second
rospy.sleep(1) # Change sleep values here to increase duration of action to be executed - increasing the time will allow longer time during which action is executed

# Testing for motion along two edges of the square trajectory
for x in range(4):
    # First edge
    if x == 0:
        dx = 1
        dy = 0
        dphi=1.5708/3

        move_rob(dphi, dx, dy)

    # Second edge
    elif x == 1:
        dx = 0
        dy = 1
        dphi=1.5708/3

        move_rob(dphi, dx, dy)

    # Third edge
    elif x == 2:
        dx = -1
        dy = 0
        dphi=1.5708/3

        move_rob(dphi, dx, dy)

    # Fourth edge
    elif x == 3:
        dx = 0
        dy = -1
        dphi=1.5708/3

        move_rob(dphi, dx, dy)

stop = [0,0,0,0]
msg = Float32MultiArray(data=stop)
pub.publish(msg)

'''
for _ in range(100):
    wz, vx, vy = velocity2twist(dphi=1.5708, dx=1, dy=0)
    u = twist2wheels(wz, vx, vy)
    msg = Float32MultiArray(data=u)
    pub.publish(msg)
    rospy.sleep(0.01)
    
stop = [0,0,0,0]
msg = Float32MultiArray(data=stop)
pub.publish(msg)
'''

'''
u = twist2wheels(wz=1.5, vx=1, vy=0)
msg = Float32MultiArray(data=u)
pub.publish(msg)
rospy.sleep(1)
stop = [0,0,0,0]
msg = Float32MultiArray(data=stop)
pub.publish(msg)
'''