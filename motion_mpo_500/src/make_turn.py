#! /usr/bin/env python

# Import dependencies
import rospy, math, numpy as np
from std_msgs.msg import Float32MultiArray
#from utilities import reset_world

# Initialize the node
rospy.init_node('make_turn_node', anonymous=True)

# Define the publisher for moving the robot
pub = rospy.Publisher('wheel_speed', Float32MultiArray, queue_size=10)

# Instructing sleep duration of 1 second
rospy.sleep(1) # Change sleep values here to increase duration of action to be executed - increasing the time will allow longer time during which action is executed

def twist2wheels(wz, vx, vy):
    '''
    Given in the ipynb notebook that,
    u = Transpose[u1 u2 u3 u4] = (1/r)* [[(-l-w), 1, -1], [(l+w), 1, 1], [(l+w), 1, -1], [(l-w), 1, 1].Transpose[w_bz v_bx v_by]
    '''
    # Plugging in the values from the ipynb notebook
    length = 0.250 # 500/2
    width = 0.274 # 548/2
    radius = 0.127 # 254/2

    # Defining intermediate_matrix for '(1/r)* [[(-l-w), 1, -1], [(l+w), 1, 1], [(l+w), 1, -1], [(l-w), 1, 1]' part
    intermediate_matrix = np.array([[(-length-width)/radius, 1/radius, -1/radius], 
                                    [(length+width)/radius, 1/radius, 1/radius], 
                                    [(length+width)/radius, 1/radius, -1/radius], 
                                    [(-length-width)/radius, 1/radius, 1/radius]])
    
    # Defining the twist_vector_matrix for storing wz, vx, vy
    twist_vector_matrix = np.array([wz, vx, vy])

    # Defining the dimensions for the twist_vector_matrix
    twist_vector_matrix.shape = (3,1)

    # Computing dot product for u, [4x3]dot[3x1] = [4x1], which is the needed dimensions for u
    u = np.dot(intermediate_matrix, twist_vector_matrix)

    return u

u = twist2wheels(wz=1.5, vx=1, vy=0)
msg = Float32MultiArray(data=u)
pub.publish(msg)
rospy.sleep(1)
stop = [0,0,0,0]
msg = Float32MultiArray(data=stop)
pub.publish(msg)