#! /usr/bin/env python

# Import dependencies
import rospy, math, numpy as np
from math import sin, cos, atan2, sqrt
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion

# Initialize the node
rospy.init_node('vaccum_cleaner', anonymous=True)

# Define a class for move function to publish linear.x and angular.z values
class VelocityController():
    def __init__(self, topic):
        self.cmd_vel = rospy.Publisher(topic, Twist, queue_size=10)
        self.max_vel = 0.65
        rospy.sleep(0.1)

    def move(self, linear_velocity=0.0, angular_velocity=0.0):
        abs_v = abs(linear_velocity)
        if abs_v <= self.max_vel:
            vx = linear_velocity
            wz = angular_velocity
        else:
            vx = linear_velocity / abs_v * self.max_vel
            wz = angular_velocity / abs_v * self.max_vel
        msg = Twist()
        msg.linear.x = vx
        msg.angular.z = wz
        self.cmd_vel.publish(msg)

# Define a class for containing subscriber that reads odometry info
class OdometryReader():
    def __init__(self, topic):
        self.pose = {}
        self.trajectory = []
        self.topic = topic
        self.subscribe()

    def callback(self, msg):
        self.pose['x'] = msg.pose.pose.position.x
        self.pose['y'] = msg.pose.pose.position.y
        self.trajectory.append((self.pose['x'], self.pose['y']))
        (_, _, self.pose['theta']) = euler_from_quaternion([msg.pose.pose.orientation.x, 
                                                            msg.pose.pose.orientation.y, 
                                                            msg.pose.pose.orientation.z, 
                                                            msg.pose.pose.orientation.w])
    def subscribe(self):
        self.subscriber = rospy.Subscriber(self.topic, Odometry, self.callback)
        rospy.sleep(0.1)

    def unregister(self):
        np.save('trajectory',self.trajectory)
        self.subscriber.unregister()


#################
### FUNCTIONS ###
#################
def normalize(angle):
    '''
    Angle received is in radians; normalized equivalent angle in 
    [-pi, pi] range is to be returned.
    'X Using atan in math'
    EDIT - Using atan2 in math since we need to compute it for 
    possible values in four quadrant for combinations of x and y
    values, rather than two quadrants as supported by atan
    '''
    sine_var = sin(angle)
    cos_var = cos(angle)
    normalized_angle = atan2(sine_var, cos_var)
    return normalized_angle

def go_to(xg, yg, thetag_degrees):
    rho = float("inf")
    thetag = math.radians(thetag_degrees)
    while rho>0.01:
        # Calculate del_x (goal - current)
        del_x = xg - odometry.pose['x']

        # Calculate del_y (goal - current)
        del_y = yg - odometry.pose['y']

        # calculate rho (formula in ipynb)
        rho = sqrt(del_x**2 + del_x**2)

        # Parse theta value
        theta = odometry.pose['theta']

        # Compute alpha (formula in ipynb)
        alpha = normalize(atan2(del_y, del_x) - theta)

        # Compute beta (formula in ipynb)
        beta = normalize(thetag - atan2(del_y,del_x))

        # Compute v and w to push to the robot
        v = k_rho * rho
        w = k_alpha * alpha + k_beta * beta
        
        # Define constant velocity
        const_vel = 0.65

        v = (v*const_vel)/abs(v)
        w = (w*const_vel)/abs(v)

        velocity.move(v, w)
        rospy.sleep(0.01)
        
k_rho = 0.3
k_alpha = 0.8
k_beta = -0.15

counter = 0

def move_spiral():
    v = 0.65
    move_radius = 0.1
    global counter

    if counter<=3:
        # while loop for checking move_radius constraint
        while move_radius<=1.10:    # Fine-tuned to 1.10 to accomodate for controller errors
            w = -v/move_radius
            velocity.move(v,w)
            move_radius = move_radius + 0.005 # This increment gave tighter turns and better efficiency
            rospy.sleep(0.1)
        counter = counter + 1
    
    elif counter>3:
        # while loop for checking move_radius constraint
        while move_radius<=2.10:
            w = -v/move_radius
            velocity.move(v,w)
            move_radius = move_radius + 0.005
            rospy.sleep(0.1)
        counter = counter + 1
    print("Counter = %d " % counter)


# Refer to the image 'final project_FINAL1.jpg for the labeled diagram
traj_waypoints = [(-3,0,90), (-1,0,-90), (1,0,-90), (3,0,-90), (-2,-7,-90), (2,-7,-90)]
'''
traj_waypoints = [(0,0,180),(-3,0,0),(-1,0,0),(1,0,0),(3,0,-90),
                  (-2,-7,-90),(2,-7,90)]
'''
from utilities import set_position, cleaned_area
set_position(0,0,0)
velocity = VelocityController('/cmd_vel')
odometry = OdometryReader('/odom')
init_time = rospy.get_time()

try:
    # Will try to emulate the spherical movement shown in the gif
    for xg, yg, thetag_degrees in traj_waypoints:
        # Parse the first waypoint in the trajectory
        # x_pos, y_pos, theta_pos = traj_waypoints[waypoints]

        '''
        In the first case, since our robot has position (0,0) and 
        we are starting from A(-3,0), thus we first move the robot
        to point A, and then we spiral, then move to point B and spiral
        and so on
        '''
    
        # Move the robot to that waypoint
        go_to(xg, yg, thetag_degrees)

        # Execute the spiral motion
        move_spiral()
        
except KeyboardInterrupt:
    pass

end_time = rospy.get_time()
velocity.move(0,0)
odometry.unregister()
t = end_time-init_time # init_time not start_time
m = int(t/60)
s = t - m*60
print("You cleaned %.2f m2 in %d minutes and %d seconds." % 
      (cleaned_area(odometry.trajectory), m, s))