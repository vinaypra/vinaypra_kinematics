import time
import math, rospy
from utilities import set_model_state, get_model_state, \
                      spawn_coke_can
from geometry_msgs.msg import Pose, Point, Quaternion

if get_model_state('coke_can').success == False:
    spawn_coke_can('coke_can', Pose(position=Point(0,1,0.22)))


model_state = get_model_state('coke_can')
print (model_state.pose.position)

time.sleep(2)
new_pose_1 = Pose(position=Point(1,0,0.22))
set_model_state('coke_can', new_pose_1)

time.sleep(2)
new_pose_2 = Pose(position=Point(2,2,0.22))
set_model_state('coke_can', new_pose_2)

time.sleep(2)
new_pose_3 = Pose(position=Point(3,3,0.22))
set_model_state('coke_can', new_pose_3)