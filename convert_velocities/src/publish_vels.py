#! /usr/bin/env python

# Import dependencies
import rospy
from geometry_msgs.msg import Twist

# Initialize node
rospy.init_node("publish_velocity_node")

# Define rate
rate = rospy.Rate(1)

# Define publisher for publishing linear and angular velocities
diff_robot_pub = rospy.Publisher('/twist_vels', Twist, queue_size=1)
diff_rob_vel = Twist() # Creating object instance

# Define velocities for differential robot
diff_rob_vel.linear.x = 0.3
diff_rob_vel.angular.z = 0.3

# Continuously publish until interrupted
while not rospy.is_shutdown():
    diff_robot_pub.publish(diff_rob_vel) # Publish x and z velocities
    rate.sleep()