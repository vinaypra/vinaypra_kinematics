#! /usr/bin/env python

# Import dependencies
import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64

'''
# Initialize node
rospy.init_node('subscribe_velocity_node')

# Define callback function to store the velocities read from the topic /twist_vels
def callback(msg):
    diff_rob_vel_new = msg
    return [diff_rob_vel_new]

# Define subscriber for /twist_vels topic
diff_robot_sub = rospy.Subscriber('/twist_vels', Twist, callback)
diff_rob_vel_new = Twist() # Creating object instance

# Defining publisher for right wheel
right_wheel_pub = rospy.Publisher('/right_wheel_controller/command', Float64, queue_size=1)
vel_right_wheel = Float64() # Creating object instance

# Defining publisher for left wheel
left_wheel_pub = rospy.Publisher('/left_wheel_controller/command', Float64, queue_size=1)
vel_left_wheel = Float64() # Creating object instance

# Define rate
rate = rospy.Rate(1)

# Continuously publish until interrupted
while not rospy.is_shutdown():
    
    # Compute right and left wheel velocities from given Twist message values
    vel_right_wheel = ((2*diff_rob_vel_new.linear.x) + (diff_rob_vel_new.angular.z*0.3))/(2*0.1)
    vel_left_wheel = ((2*diff_rob_vel_new.linear.x) - (diff_rob_vel_new.angular.z*0.3))/(2*0.1)

    # Publish the velocities
    right_wheel_pub.publish(vel_right_wheel)
    left_wheel_pub.publish(vel_left_wheel)
    rate.sleep()
'''
'''
The callback function wasn't returning the values from the /twist_vels 
topic so the creating a class sseemed the only option.
'''
class MoveDiffRobot():
    def __init__(self):
        # Define subscriber for /twist_vels topic
        self.diff_robot_sub = rospy.Subscriber('/twist_vels', Twist, self.callback)
        self.diff_rob_vel_new = Twist() # Creating object instance

        # Defining publisher for right wheel
        self.right_wheel_pub = rospy.Publisher('/right_wheel_controller/command', Float64, queue_size=1)
        self.vel_right_wheel = Float64() # Creating object instance

        # Defining publisher for left wheel
        self.left_wheel_pub = rospy.Publisher('/left_wheel_controller/command', Float64, queue_size=1)
        self.vel_left_wheel = Float64() # Creating object instance

        # Define rate
        self.rate = rospy.Rate(1)
        #self.rate = rospy.Rate(5)

    def callback(self,msg):
        self.diff_rob_vel_new = msg

    def pub_wheel_velocities(self):
        while not rospy.is_shutdown():
            # Compute right and left wheel velocities from given Twist message values
            vel_right_wheel = ((2*self.diff_rob_vel_new.linear.x) + (self.diff_rob_vel_new.angular.z*0.3))/(2*0.1)
            vel_left_wheel = ((2*self.diff_rob_vel_new.linear.x) - (self.diff_rob_vel_new.angular.z*0.3))/(2*0.1)

            # Publish the velocities
            self.right_wheel_pub.publish(vel_right_wheel)
            self.left_wheel_pub.publish(vel_left_wheel)
            self.rate.sleep()

if __name__ == '__main__':
    # Initialize node
    rospy.init_node('subscribe_velocity_node', anonymous=True)

    # Create an instance of the class
    move_diff_rob_obj = MoveDiffRobot()

    # Publish wheel velocities to the right and left wheels
    move_diff_rob_obj.pub_wheel_velocities()

    '''
    try:
        # Publish wheel velocities to the right and left wheels
        move_diff_rob_obj.pub_wheel_velocities()

    except rospy.ROSInterruptException:
        pass
    '''