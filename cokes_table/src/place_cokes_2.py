import math, rospy
from utilities import spawn_coke_can, spawn_table, \
                      pause_physics, unpause_physics, set_model_state
from geometry_msgs.msg import Pose, Point, Quaternion
from tf.transformations import quaternion_about_axis, quaternion_multiply

unpause_physics()
spawn_table('table', Pose(position=Point(1,0,0)))

for i in range(12):
    spawn_coke_can('coke_can_'+str(i), Pose(position=Point(1,0,1.25)))
    #spawn_coke_can('coke_can_'+str(i), Pose(position=Point(0,0,1)))
    rospy.sleep(1.0)

'''
Since there are 12 coke cans and they are evenly spread out in a
circle, therefore we have to position them at different angular 
positions over 360-degrees.abs
360/12 = 30
Thus, each can will be at a difference of 30-degrees from the last 
can.
For example, if first can is at 0-degree, then second is at 30-degrees,
the third can is at 60-degrees, and so on for 12th can at 330-degrees.

Therefore:

STEP 1 - Move cans to a circle of some radius (1-2 cans 
         fell off the table for radius around 0.4)
STEP 2 - Apply rotations as previously done to make coke cans lie 
         sideways
'''
for angle_in_deg in range(0,360,30):
    '''
    For X-Coordinate - radius defined as 0.35 for all cans to be on the table, 
    angle converted from degrees to radians and 1 added at 
    end because table center is at x = 1
    For Y-Coordinate - same, except no 1 at the end since table center 
    is at y = 0.
    '''
    x = 0.4 * math.cos(math.radians(angle_in_deg)) + 1
    y = 0.4 * math.sin(math.radians(angle_in_deg))

    # Calculating serial number of sorts for displacing later on based on required position around the circle
    j = angle_in_deg/30
    coke_can_num = 'coke_can_'+str(j)

    # Setting position for displaced Coke cans
    position=Point(x,y,1.05)

    # Rotating the Coke cans sideways (as done in previous exercises)
    q_z = quaternion_about_axis(math.radians(angle_in_deg), (0,0,1))
    q_y = quaternion_about_axis(math.radians(90), (0,1,0))
    q_zy = quaternion_multiply(q_z, q_y)
    orientation = Quaternion(*q_zy)

    # Changing state of Coke cans based on numbers for different positions and orientations
    set_model_state(coke_can_num, Pose(position, orientation))
    
    rospy.sleep(0.1)

    '''
    # For default upright position of Coke cans (from tf2/Tutuorials/Quaternions on ROS Wiki)
    quat_ideal = [0, 0, 0, 1]
    # Setting orientation for displaced Coke cans
    orientation = Quaternion(quat_ideal[0], quat_ideal[1], quat_ideal[2], quat_ideal[3])
    # Changing state of Coke cans based on numbers for different positions
    set_model_state(coke_can_num, Pose(position, orientation))
    rospy.sleep(0.1)
    '''