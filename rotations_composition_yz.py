import math, rospy
from utilities import set_model_state, get_model_state, \
                      pause_physics, unpause_physics, spawn_coke_can
from geometry_msgs.msg import Pose, Point, Quaternion
from tf.transformations import quaternion_about_axis
from tf.transformations import quaternion_multiply

# Specifying position of object in Gazebo here
position = Point(x=0.5, y=0, z=0.25)

''' Use the code block below to spawn a Coke can in Gazebo, in case it is missing in there
if get_model_state('coke_can').success == False:
    spawn_coke_can('coke_can', Pose(position))
    #spawn_coke_can('coke_can', Pose(position=Point(0.5,0,0.5)))
'''
# Specifying the angle: 90-degree rotation here
for angle in range(0,90,3):
    q_y = quaternion_about_axis(math.radians(angle), (0,1,0)) # Specifying rotation along y-axis here
    orientation = Quaternion(*q_y)
    set_model_state('coke_can', Pose(position, orientation)) # Coke can to be rotated
    rospy.sleep(0.1)

for angle in range(0,90,3):
    q_z = quaternion_about_axis(math.radians(angle), (0,0,1))
    q_yz = quaternion_multiply(q_y, q_z) # Order of multiplication is first y and then for z axis
    orientation = Quaternion(*q_yz)
    set_model_state('coke_can', Pose(position, orientation))
    rospy.sleep(0.1)