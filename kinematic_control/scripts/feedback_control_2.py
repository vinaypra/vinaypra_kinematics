#! /usr/bin/env python

# Import dependencies
import rospy, math, numpy as np
from math import sin, cos, atan2, sqrt #, atan
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion

# Initialize the node
rospy.init_node('kinematic_controller', anonymous=True)

'''
# Define list to store the waypoints
traj_waypoints = []
'''

# Define a class for move function to publish linear.x and angular.z values
class VelocityController():
    def __init__(self, topic):
        self.cmd_vel = rospy.Publisher(topic, Twist, queue_size=10)
        rospy.sleep(0.1)

    def move(self, linear_velocity=0.0, angular_velocity=0.0):
        msg = Twist()
        msg.linear.x = linear_velocity
        msg.angular.z = angular_velocity
        self.cmd_vel.publish(msg)

# Define a class for containing subscriber that reads odometry info
class OdometryReader():
    def __init__(self, topic):
        self.pose = {}
        self.trajectory = []
        self.topic = topic
        self.subscribe()

    def callback(self, msg):
        self.pose['x'] = msg.pose.pose.position.x
        self.pose['y'] = msg.pose.pose.position.y
        self.trajectory.append((self.pose['x'], self.pose['y']))
        (_, _, self.pose['theta']) = euler_from_quaternion([msg.pose.pose.orientation.x, 
                                                            msg.pose.pose.orientation.y, 
                                                            msg.pose.pose.orientation.z, 
                                                            msg.pose.pose.orientation.w])
    def subscribe(self):
        self.subscriber = rospy.Subscriber(self.topic, Odometry, self.callback)
        rospy.sleep(0.1)

    def unregister(self):
        np.save('trajectory',self.trajectory)
        self.subscriber.unregister()

def normalize(angle):
    '''
    Angle received is in radians; normalized equivalent angle in 
    [-pi, pi] range is to be returned.
    'X Using atan in math'
    EDIT - Using atan2 in math since we need to compute it for 
    possible values in four quadrant for combinations of x and y
    values, rather than two quadrants as supported by atan
    '''
    sine_var = sin(angle)
    cos_var = cos(angle)
    normalized_angle = atan2(sine_var, cos_var)
    return normalized_angle

'''
# Test code for the normalize() function
import random
for _ in range(10):
    angle = (random.random()-0.5)*2*math.pi
    new_angle = angle + (random.randint(0,10)-5) * 2*math.pi
    norm_angle = normalize(new_angle)
    print('%9.4f %9.4f %9.4f' %(angle, new_angle, norm_angle))
'''


def go_to(xg, yg, thetag_degrees):
    rho = float("inf")
    thetag = math.radians(thetag_degrees)
    while rho>0.01:
        # Calculate del_x (goal - current)
        del_x = xg - odometry.pose['x']

        # Calculate del_y (goal - current)
        del_y = yg - odometry.pose['y']

        # calculate rho (formula in ipynb)
        rho = sqrt(del_x**2 + del_x**2)

        # Parse theta value
        theta = odometry.pose['theta']

        # Compute alpha (formula in ipynb)
        alpha = normalize(atan2(del_y, del_x) - theta)

        # Compute beta (fixed for correct trajectory execution)
        beta = normalize(thetag - atan2(del_y,del_x))

        # Compute v and w to push to the robot
        v = k_rho * rho
        w = k_alpha * alpha + k_beta * beta
        
        # Define constant velocity
        const_vel = 0.3

        v = (v*const_vel)/abs(v)
        w = (w*const_vel)/abs(v)

        velocity.move(v, w)
        rospy.sleep(0.01)
        
k_rho = 0.3
k_alpha = 0.8
k_beta = -0.15


from utilities import set_position
# Set robot initial position and orientation as shown in the problem statement
set_position(0,0,0)


'''
The class VelocityController takes in the topic name like /cmd_vel 
or /odom as input for publishers or subscribers later on in the classes
'''

velocity = VelocityController('/cmd_vel')
odometry = OdometryReader('/odom')
rospy.sleep(1)

'''
go_to(3, 1, 0)
'''

waypoints = [(1,-1,-90),(2,-2,0),(3,-2,0),(4,-1,90),(3.5,-0.5,180),
             (3,0,90),(3,1,90),(2,1,-90),(1,0,180),(0,0,180)]

for xg, yg, thetag in waypoints:
    go_to(xg, yg, thetag)


velocity.move(0,0)
odometry.unregister()
error = math.hypot(odometry.pose['x'], odometry.pose['y'])
print('Final positioning error is %.2fm' % error)
