#! /usr/bin/env python

# Import dependencies
import rospy, math, numpy as np
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion

# Initialize the node
rospy.init_node('kinematic_controller', anonymous=True)

# Define a class for move function to publish linear.x and angular.z values
class VelocityController():
    def __init__(self, topic):
        self.cmd_vel = rospy.Publisher(topic, Twist, queue_size=10)
        rospy.sleep(0.1)

    def move(self, linear_velocity=0.0, angular_velocity=0.0):
        msg = Twist()
        msg.linear.x = linear_velocity
        msg.angular.z = angular_velocity
        self.cmd_vel.publish(msg)

# Define a class for containing subscriber that reads odometry info
class OdometryReader():
    def __init__(self, topic):
        self.pose = {}
        self.trajectory = []
        self.topic = topic
        self.subscribe()

    def callback(self, msg):
        self.pose['x'] = msg.pose.pose.position.x
        self.pose['y'] = msg.pose.pose.position.y
        self.trajectory.append((self.pose['x'], self.pose['y']))
        (_, _, self.pose['theta']) = euler_from_quaternion([msg.pose.pose.orientation.x, 
                                                            msg.pose.pose.orientation.y, 
                                                            msg.pose.pose.orientation.z, 
                                                            msg.pose.pose.orientation.w])
    def subscribe(self):
        self.subscriber = rospy.Subscriber(self.topic, Odometry, self.callback)
        rospy.sleep(0.1)

    def unregister(self):
        np.save('trajectory',self.trajectory)
        self.subscriber.unregister()

from utilities import set_position
# Set robot initial position and orientation as shown in the problem statement
set_position(0,0,0)

'''
The class VelocityController takes in the topic name like /cmd_vel 
or /odom as input for publishers or subscribers later on in the classes
'''
velocity = VelocityController('/cmd_vel')
odometry = OdometryReader('/odom')
rospy.sleep(1)

#################
### YOUR CODE ###

# Initialize necessary variable for turns
traj_radius = 0
traj_theta = 0
vel_max = 0.65 # Given in the ipynb that 'maximum speed of 0.65 m/s (25.6 in/s)'

for i in range(11): #11 movements in the path
    if i == 0:
        # Move right, R = 1, theta = 90 degrees
        traj_radius = 1
        traj_theta = 90

        v = vel_max
        w = -vel_max/traj_radius # -ve for correct rotation

        # Push values of v and w to the robot
        velocity.move(v,w)

        '''
        Calculate travel time for TurtleBot so that adequate interval 
        is allowed before publishing any other move commands so as 
        to allow the robot to complete the trajectory

        arc_len = traj_radius*((2*pi*traj_theta)/360)
        '''
        # Compute the distance robot has to travel
        arc_len = traj_radius*(math.radians(traj_theta))

        # Compute the time needed to travel this distance
        trav_time = arc_len/vel_max
        rospy.sleep(trav_time)

    if i == 1:
        # Move left, R = 1, theta = 90 degrees
        traj_radius = 1
        traj_theta = 90

        v = vel_max
        w = vel_max/traj_radius

        # Push values of v and w to the robot
        velocity.move(v,w)

        # Compute the distance robot has to travel
        arc_len = traj_radius*(math.radians(traj_theta))

        # Compute the time needed to travel this distance
        trav_time = arc_len/vel_max
        rospy.sleep(trav_time)

    if i == 2:
        # Move straight, R = inf, theta = N/A
        traj_radius = float('inf')

        v = vel_max
        w = vel_max/traj_radius

        # Push values of v and w to the robot
        velocity.move(v,w)

        # Compute the distance robot has to travel (1, in this case)
        arc_len = 1

        # Compute the time needed to travel this distance
        trav_time = arc_len/vel_max
        rospy.sleep(trav_time)

    if i == 3:
        # Move left, R = 1, theta = 90 degrees
        traj_radius = 1
        traj_theta = 90

        v = vel_max
        w = vel_max/traj_radius

        # Push values of v and w to the robot
        velocity.move(v,w)

        # Compute the distance robot has to travel
        arc_len = traj_radius*(math.radians(traj_theta))

        # Compute the time needed to travel this distance
        trav_time = arc_len/vel_max
        rospy.sleep(trav_time)

    if i == 4:
        # Move left, R = 0.5, theta = 90 degrees
        traj_radius = 0.5
        traj_theta = 90

        v = vel_max
        w = vel_max/traj_radius

        # Push values of v and w to the robot
        velocity.move(v,w)

        # Compute the distance robot has to travel
        arc_len = traj_radius*(math.radians(traj_theta))

        # Compute the time needed to travel this distance
        trav_time = arc_len/vel_max
        rospy.sleep(trav_time)

    if i == 5:
        # Move right, R = 0.5, theta = 90 degrees
        traj_radius = 0.5
        traj_theta = 90

        v = vel_max
        w = -vel_max/traj_radius # -ve for correct rotation

        # Push values of v and w to the robot
        velocity.move(v,w)

        # Compute the distance robot has to travel
        arc_len = traj_radius*(math.radians(traj_theta))

        # Compute the time needed to travel this distance
        trav_time = arc_len/vel_max
        rospy.sleep(trav_time)

    if i == 6:
        # Move straight, R = inf, theta = N/A
        traj_radius = float('inf')

        v = vel_max
        w = vel_max/traj_radius

        # Push values of v and w to the robot
        velocity.move(v,w)

        # Compute the distance robot has to travel (1, in this case)
        arc_len = 1

        # Compute the time needed to travel this distance
        trav_time = arc_len/vel_max
        rospy.sleep(trav_time)

    if i == 7:
        # Move left, R = 0.5, theta = 90 degrees
        traj_radius = 0.5
        traj_theta = 90

        v = vel_max
        w = vel_max/traj_radius

        # Push values of v and w to the robot
        velocity.move(v,w)

        # Compute the distance robot has to travel
        arc_len = traj_radius*(math.radians(traj_theta))

        # Compute the time needed to travel this distance
        trav_time = arc_len/vel_max
        rospy.sleep(trav_time)

    if i == 8:
        # Move left, R = 0.5, theta = 90 degrees
        traj_radius = 0.5
        traj_theta = 90

        v = vel_max
        w = vel_max/traj_radius

        # Push values of v and w to the robot
        velocity.move(v,w)

        # Compute the distance robot has to travel
        arc_len = traj_radius*(math.radians(traj_theta))

        # Compute the time needed to travel this distance
        trav_time = arc_len/vel_max
        rospy.sleep(trav_time)

    if i == 9:
        # Move right, R = 1, theta = 90 degrees
        traj_radius = 1
        traj_theta = 90

        v = vel_max
        w = -vel_max/traj_radius # -ve for correct rotation

        # Push values of v and w to the robot
        velocity.move(v,w)

        # Compute the distance robot has to travel
        arc_len = traj_radius*(math.radians(traj_theta))

        # Compute the time needed to travel this distance
        trav_time = arc_len/vel_max
        rospy.sleep(trav_time)

    if i == 10:
        # Move straight, R = inf, theta = N/A
        traj_radius = float('inf')

        v = vel_max
        w = vel_max/traj_radius

        # Push values of v and w to the robot
        velocity.move(v,w)

        # Compute the distance robot has to travel (1, in this case)
        arc_len = 1

        # Compute the time needed to travel this distance
        trav_time = arc_len/vel_max
        rospy.sleep(trav_time)

#################

velocity.move(0,0)
odometry.unregister()
error = math.hypot(odometry.pose['x'], odometry.pose['y'])
print('Final positioning error is %.2fm' % error)