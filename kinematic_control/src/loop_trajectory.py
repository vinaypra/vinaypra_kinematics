'''
# For the open loop controller

import numpy as np
from plot_functions import plot_trajectory
from plot_functions import plot_error

path_to_file = '/home/user/catkin_ws/src/vinaypra_kinematics/kinematic_control/scripts/'
open_loop_trajectory = np.load(path_to_file+'trajectory.npy')
waypoints = np.load(path_to_file+'traj_waypoints.npy')
plot_trajectory(open_loop_trajectory, waypoints)

ref_points = [(0,0),(1,-1),(2,-2),(3,-2),(4,-1),(3.5,-0.5),(3,0),(3,1),(2,1),(1,0),(0,0)]
plot_error(waypoints, ref_points)
'''
# For the feedback controller

# Import dependencies
import numpy as np
from plot_functions import plot_trajectory

# Modified path to correct package address
path_to_file = '/home/user/catkin_ws/src/vinaypra_kinematics/kinematic_control/scripts/'
closed_loop_trajectory = np.load(path_to_file+'trajectory.npy')
plot_trajectory(closed_loop_trajectory)